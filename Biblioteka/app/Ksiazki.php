<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ksiazki extends Model
{
    protected $fillable = [
      'title','body'
    ];

    public function users()
    {
        return $this->hasMany(User::class, 'id');
    }
}

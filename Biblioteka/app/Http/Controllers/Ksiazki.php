<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class Ksiazki extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $wyswietllisteksiazek = \App\Ksiazki::all();

        return view('lista', ['wyswietllisteksiazek' => $wyswietllisteksiazek]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('stworz');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $ksiazka = new \App\Ksiazki();
        $ksiazka->title = $request->title;
        $ksiazka->body = $request->body;
        $ksiazka->save();
        return redirect('/lista');
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @return Response
     */
    public function dodajDoCzytelnika(Request $request)
    {
        $user = User::find(Auth::user()->getAuthIdentifier());
        $user->books()->attach($request->addBook);
        return redirect('/lista');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function mojeksiazki($id)
    {
        $user = User::with('books')->findOrFail($id);

        return view('ksiazkiczytelnika',['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function odłącz(Request $request, $id)
    {
        $user = User::find(Auth::user()->getAuthIdentifier());
        $user->books()->detach($request->addBook);

        return redirect("/mojeksiazki/".Auth::user()->getAuthIdentifier());
    }
}

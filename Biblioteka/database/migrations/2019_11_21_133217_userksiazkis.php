<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Userksiazkis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userksiazkis', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('ksiazkis_id');
            $table->foreign('user_id')->references('id')->on('ksiazkis')->onDelete('cascade');
            $table->foreign('ksiazkis_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userksiazkis');
    }
}

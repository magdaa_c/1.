@extends('layouts.app')

@section('content')
    <div class="container">
        <a href="{{ route('ksiazki.stworz') }}" class="btn btn-info">Dodaj książkę</a>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Tytuł</th>
                <th scope="col">Opis</th>
                <th scope="col">Data powstania</th>
                <th scope="col">Usuń</th>
            </tr>
            </thead>
            <tbody>
            @foreach($wyswietllisteksiazek as $book)
                <tr>
                    <th scope="row">{{ $book->id }}</th>
                    <td>{{ $book->title }}</td>
                    <td>{{ $book->body }}</td>
                    <td>{{ $book->created_at->diffForHumans() }}</td>
                    <td>
                        <form action="{{ route('ksiazki.dodajDoCzytelnika') }}" method="post">
                            @csrf
                            <input type="hidden" value="{{ $book->id }}" name="addBook">
                            <button class="btn btn-outline-success" type="submit">Dodaj</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

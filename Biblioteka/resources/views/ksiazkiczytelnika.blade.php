@extends('layouts.app')

@section('content')
    <div class="container">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Tytuł</th>
                <th scope="col">Opis</th>
                <th scope="col">Data powstania</th>
                <th scope="col">Usuń</th>
            </tr>
            </thead>
            <tbody>
            @foreach($user->books as $book)
                <tr>
                    <th scope="row">{{ $book->id }}</th>
                    <td>{{ $book->title }}</td>
                    <td>{{ $book->body }}</td>
                    <td>{{ $book->created_at->diffForHumans() }}</td>
                    <td>
                        <form action="{{ route('ksiazki.odłącz', $book->id) }}" method="post">
                            @csrf
                            <input type="hidden" value="{{ $book->id }}" name="addBook">
                            <button class="btn btn-outline-danger">Usuń</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

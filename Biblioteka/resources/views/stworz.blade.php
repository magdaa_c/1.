@extends('layouts.app')

@section('content')
    <div class="container">
        <a href="{{ route('ksiazki.stworz') }}" class="btn btn-info">Dodaj książkę</a>
        <form method="post" action="{{ route('ksiazki.dodaj') }}">
            @csrf
            <div class="form-group">
                <label for="title">Tytuł</label>
                <input type="text" class="form-control" id="title" name="title" placeholder="Enter title">
            </div>
            <div class="form-group">
                <label for="body">Opis</label>
                <textarea type="text" class="form-control" id="body" name="body" placeholder="Enter body">
                </textarea>
            </div>
            <button type="submit" class="btn btn-primary">Dodaj</button>
        </form>
    </div>
@endsection
